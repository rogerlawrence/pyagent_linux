import sys
import os
import platform
import pprint
import globals
import helpers.helpers_os as helpers_os
import subprocess
# INSTALL POSTGRES SUPPORT
# apt-get install python-psycopg2
#
# -------------------------------------------------------------------------------------------------
# SCREENCONNECT:
# 
# -------------------------------------------------------------------------------------------------
def install_screenconnect():
    print("Installing JRE Headless (default-jre-headless)...")
    response = helpers_os.run_go(["apt-get", "-y", "install", "default-jre-headless"])
    if not response: 
        print("Error: Installing JRE Headless")
        return False 

    print("Running autoremove (apt-get autoremove)...")
    response = helpers_os.run_go(["apt-get", "autoremove"])
    if not response:
        print("Error: Issue with autoremove")
        return False

    print("Installing Default JRE (default-jre)...")
    response = helpers_os.run_go(["apt-get", "-y", "install", "default-jre"])
    if not response:
        print("Error: Installing Default JRE")
        return False

    if not os.path.isfile("/tmp/screenconnect.deb"):
        print("Downloading ScreenConnect...")
        response = helpers_os.run_go(["/usr/bin/wget", "-O", "/tmp/screenconnect.deb", globals.DL_SC])
        if not response:
            print("Error: Downloading ScreenConnect")
            return False
    else:
        print("ScreenConnect already exists in /tmp DIR")

    print("Installing ScreenConnect...")
    response = helpers_os.run_go(["/usr/bin/dpkg", "-i", "/tmp/screenconnect.deb"])
    if not response:
        print("Error: Installing ScreenConnect Package")
        return False
    
    print("Complete")
    return True

# -------------------------------------------------------------------------------------------------
# ZABBIX:
#
# -------------------------------------------------------------------------------------------------
def install_zabbix(version):
    hostname = platform.node()
    correct = input("\n Is this this the correct Serial? " + hostname + "  (Y/N) ").upper()
    if correct == "N":
        print("Please set hostname as serial of device before proceeding.  Exiting now.")
        return False

    print("Downloading Zabbix Repo...")
    
    # VERSION CHECK
    if version == 18:
        zx_filename = "zabbix-release_5.0-1+bionic_all.deb"
        zx_dl = globals.DL_ZX_18
    else:
        zx_filename = "zabbix-release_5.0-1+xenial_all.deb"
        zx_dl = globals.DL_ZX_16
        
    response = helpers_os.run_go(["/usr/bin/wget", "-O", "/tmp/" + zx_filename, zx_dl])
    if not response:
        print("Error: Downloading Zabbix Repo")
        return False

    print("Installing Zabbix Repo...")
    response = helpers_os.run_go(["/usr/bin/dpkg", "-i", "/tmp/" + zx_filename])
    if not response:
        print("Error: Installing Zabbix Repo")
        return False
        
    print("Updating APT...")
    response = helpers_os.run_go(["apt-get", "update"])
    if not response:
        print("Error: Updating APT")
        return False

    print("Installing Zabbix Agent...")
    response = helpers_os.run_go(["apt-get", "-y", "install", "zabbix-agent"])
    if not response:
        print("Error: Installing Zabbix Agent")
        return False

    print("Stopping Zabbix Service...")
    response = helpers_os.run_go(["/usr/sbin/service", "zabbix-agent", "stop"])
    if not response:
        print("Error: Stopping Zabbix Agent Service")
        return False

    print("Downloading Config Template...")
    response = helpers_os.run_go(["/usr/bin/wget", "-O", globals.ZX_CONF_FILE, globals.DL_ZX_CONF])
    if not response:
        print("Error: Downloading Zabbix Config Template")
        return False
    # UPDATE Config
    config = []
    with open(globals.ZX_CONF_FILE) as f:
            content = f.read().splitlines()
            for line in content:
                    if "Server=" in line or "ServerActive=" in line:
                            config.append(line + globals.CT_AGENT_URL)
                    elif "Hostname=" in line:
                            config.append(line + hostname + "-" + hostname)
                    else:
                            config.append(line)

    with open(globals.ZX_CONF_FILE, 'w') as f:                                
            f.writelines("%s\n" % line for line in config)

    print("Starting Zabbix Service...")
    response = helpers_os.run_go(["/usr/sbin/service", "zabbix-agent", "start"])
    if not response: 
        print("Error: Stopping Zabbix Service")
        return False

    print("Complete!")
    return True

# -------------------------------------------------------------------------------------------------
# DOCKER:
# -------------------------------------------------------------------------------------------------
def install_docker():
    print("Installing Docker...")
    response = helpers_os.run_go(["apt-get", "-y", "install", "docker.io"])
    if not response:
        print("Error: Installing Docker")
        return False

    print("Making it easier to use Docker...")
    response1 = helpers_os.run_go(["ln", "-sf", "/usr/bin/docker.io", "/usr/local/bin/docker"])
    response2 = helpers_os.run_go(["sed", "-i", "'$acomplete -F _docker docker'", "/etc/bash_completion.d/docker"])
    if not response1 or not response2:
        print("Error: Making it easier to use Docker")
        return False

    print("Setting up the directories...")
    response1 = helpers_os.run_go(["mkdir", "-p", "/docker/docker/"])
    response2 = helpers_os.run_go(["chmod", "777", "/docker"])
    if not response1 or not response2:
        print("Error: Setting up the directories")
        return False

    print("Complete!")
    return True

# -------------------------------------------------------------------------------------------------
# GUAC:
# -------------------------------------------------------------------------------------------------
def install_guac():
    print("Installing Guac")
    response = helpers_os.run_go(globals.GM_CREATE)
    if not response:
        print("Error: Problem creating docker!")
        return False

    print("Getting container ID...")
    response = get_guac_id()

    if not response:
        print("Error: Getting Container ID")
        return False
    else:
        container_id = response

    if container_id:
        print("Starting Guac Docker")
        response = helpers_os.run_go(["docker", "start", container_id])
    else:
        print("Error: Could not Start Guac Docker")
        return False
    print("Complete!")
    return True

def get_guac_id():
    container_id = False
    response = helpers_os.run("docker ps -a", True)

    if response:
        lines = response.split("\n")
        for line in lines:
            if "oznu/guacamole:latest" in line:
                container_id = line.split(" ")[0]
    return container_id

def check_guac():
    up = False
    response = helpers_os.run("docker ps -a", True)

    if response:
        lines = response.split("\n")
        for line in lines:
            if "oznu/guacamole:latest" in line:
                if "Up" in line:
                    up = True

    return up

def reset_guac():
    print("Getting container ID...")
    container_id = get_guac_id()
    if not container_id:
        print("Error: Getting Container ID")
        return False

    response = helpers_os.run_go(["docker", "stop", container_id])
    if not response:
        print("Error: Could not stop Guac Docker")
        return False
    
    print("Removing Guac Docker...")
    remove_guac()

    print("Pulling New Guac Docker...")
    install_guac()
    return True

def remove_guac():
    print("Getting container ID...")
    container_id = get_guac_id()
    if not container_id:
        print("Error: Getting Container ID")
        return False

    response = helpers_os.run_go(["docker", "stop", container_id])
    if not response:
        print("Error: Could not stop Guac Docker")
        return False       
    
    print("Removing Guac Docker...")
    response = helpers_os.run_go(["docker", "rm", container_id])
    if not response:
        print("Error: Could not remove Guac Docker")
        return False

    return True
# -------------------------------------------------------------------------------------------------
# Pyupdate cronjob:
# -------------------------------------------------------------------------------------------------

def install_cronjob():

    file_name = '/etc/cron.hourly/chartec2'
    if not os.path.exists(file_name):
        f = open(file_name, 'a+')
        f.write('/usr/bin/python3 /usr/share/chartec/pyagent_linux/pyagent.py --update pyupdate')
        f.close()
        return True
    else:
        response = run_go(["rm", file_name])
        if not response:
            print("Error: Removing Cron File")
            return False
        else:
            f = open(file_name, 'a+')
            f.write('/usr/bin/python3 /usr/share/chartec/pyagent_linux/pyagent.py --update pyupdate')
            f.close()
            return True
    return  

def run_go(command_list):
    """ FUNCTION run_go(<command>)
        @param list command_list
        @return output
    """
    try:
        response = subprocess.check_output(command_list)     
    except Exception as e:
        return e
    return response if response else True


# -------------------------------------------------------------------------------------------------
# Pyupdate check version & update:
# -------------------------------------------------------------------------------------------------

def check_pyupdate_version():
    # Run 'python pyupdate --version' and capture the output
    version_output = subprocess.check_output(['python3', 'pyupdate.py', '--version'], universal_newlines=True)

    # Extract the version number (assuming it's a float)
    myversion = float(version_output.strip())

    import urllib.request, json 
    with urllib.request.urlopen("https://api.chartec.net/agent/version/version") as url:
        data = json.loads(url.read().decode())
        current_version = float(data['pyupdate_linux'])
        

    # Check if bdr version is lower than current version
    if myversion < current_version:
        print(myversion)
        print("<")
        print(current_version)
        print("pyupdate my version is less than current version Proceeding with update...")
        
        # Delete the 'pyupdate' folder
        subprocess.call(['rm', 'pyupdate.py'])

        # Clone the Git repository
        subprocess.call(['git', 'clone', 'https://erwin_juarez@bitbucket.org/rogerlawrence/pyupdate_linux.git'])
        subprocess.call(['mv', 'pyupdate_linux/pyupdate.py', 'pyupdate.py'])
        subprocess.call(['rm', '-fr', 'pyupdate_linux'])
        print('updated pyagent')
    else: 
        print('pyupdate is already up to date.')

 
    version_output = subprocess.check_output(['python3', 'pyupdate.py', '--version'], universal_newlines=True)
    myversion = float(version_output.strip())

    if myversion > 1.0:
       print("version is greater than 1.0 install sudoers..")
       subprocess.check_output(['python3', 'pyupdate.py', '--install', 'sudoers'], universal_newlines=True)
        


# -------------------------------------------------------------------------------------------------
# 
# -------------------------------------------------------------------------------------------------            