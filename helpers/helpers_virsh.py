import sys
import os
import platform
import xml.etree.ElementTree as ET
import json
import pprint
import globals
import helpers.helpers_os as helpers_os
# -------------------------------------------------------------------------------------------------
# 
# 
# -------------------------------------------------------------------------------------------------
def get_vms():
    response = helpers_os.run("virsh list", True)

    if response:
        lines = response.split("\n")
        line_num = 0
        vm_obj = []
        for line in lines:
            line_num +=1
            if line_num == 1 or line_num == 2:
                continue
            if line:
                line_split = line.split("     ")[1]
                vm_status = line_split.split(" ")[1]
                vm_id = line_split.split(" ")[0]
                
                #print("Id: " + vm_id)
                #print("Status: " + vm_status)
                obj = {
                    "id": vm_id,
                    "status": vm_status
                }

                tree = ET.parse(globals.QU_DIR + vm_id + ".xml")
                root = tree.getroot()

                for item in root:
                    if item.tag == "memory":
                        obj['memory_kib'] = item.text
                    elif item.tag == "vcpu":
                        obj['vcpu'] = item.text
                    elif item.tag == "devices":
                        for device in item:
                            if device.tag == "graphics":
                                obj['vnc_password'] = device.attrib['passwd']
                                #print(device.attrib['passwd'])

                try:
                    vnc_port = helpers_os.run("virsh vncdisplay " + vm_id, True).split("\n")[0].split(":")[1]
                    if len(vnc_port) > 1:
                        obj['vnc_port'] = "59" + vnc_port
                    else:
                        obj['vnc_port'] = "590" + vnc_port
                except Exception:
                    obj['vnc_port'] = False
            
                vm_obj.append(obj)
                #print(line)
    return vm_obj