import sys
import globals
import helpers.helpers_os as helpers_os
import helpers.helpers_installers as helpers_installers


def tunnel_create(out_port, local_port):
    out_port = str(out_port)
    local_port = str(local_port)

    if local_port == 8080:
        check = helpers_installers.check_guac()
        if not check:
            print("Please start Guac docker first!")
            return

    command = ["ssh", "-R", local_port + ":localhost:"+ out_port, "-l", "replibit", globals.CT_RSSH_IP, "-p", globals.CT_RSSH_PORT]
    
    run = helpers_os.run_go(command)
    if not run:
        print("Error: Creating Tunnel")
    else:
        print("Tunnel Created")
    
    return

    
