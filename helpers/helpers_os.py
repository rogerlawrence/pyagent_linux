import subprocess
import os


def run(arg, output=False, debug=False):
    """ FUNCTION: run(<argument>)
        @param string arg
        @return response
    """
    try:
        # response = subprocess.Popen(
        #         arg,
        #         shell=True,
        #         stdin=None,
        #         #stdout=open(os.devnull, "wb"),
        #         executable="/bin/bash")

        response = os.popen(arg)
        #response.wait()
    except Exception as e:
        return str(e) if debug else False
    return response.read() if output else True


def run_go(command_list):
    """ FUNCTION run_go(<command>)
        @param list command_list
        @return output
    """
    try:
        response = subprocess.check_output(command_list)     
    except Exception as e:
        return e
    return response if response else True

# -------------------------------------------------------------------------------------------------
# STORAGE:
# -------------------------------------------------------------------------------------------------


def get_zfs(directory):
    """ FUNCTION get_zfs(<directory or to find>)
        @param string directory (Example tank, tank/admin)
        @return output
    """
    #response = run("zfs list", True)
    
    
    #print(response)
    #lines = response.split("\n")
    
    
    # obj = {}
    # lines = response.split("\n")
    # for line in lines:
    #     split = line.split(" ")
    #     #split = filter(None, split)
    #     for l in split:
    #         if l == directory:
    #             clean_line = list(filter(None, split))
    #             obj['total'] = clean_line[2]
    #             obj['used'] = clean_line[1]
    #             obj['total_bytes'] = get_bytes(obj['total'])
    #             obj['used_bytes'] = get_bytes(obj['used'])
    #             obj['used_percent'] = (obj['used_bytes'] / obj['total_bytes']) * 100
    
    response = run("zpool list", True)

    obj = {}
    lines = response.split("\n")
    for line in lines:
        split = line.split(" ")
        for l in split:
            if l == directory:
                clean_line = list(filter(None, split))
                obj['total'] = clean_line[1]
                obj['used'] = clean_line[2]
                obj['free'] = clean_line[3]
                obj['frag_percent'] = clean_line[5]
                obj['used_percent'] = clean_line[6]
                obj['health'] = clean_line[9] if len(clean_line) > 10 else clean_line[8]
                obj['total_bytes'] = get_bytes(obj['total'])
                obj['used_bytes'] = get_bytes(obj['used'])
    
    return obj

def get_zfs_list(directory):
    """ FUNCTION get_zfs(<directory or to find>)
        @param string directory (Example tank, tank/admin)
        @return output
    """
    response = run("zfs list", True)
    
    
    #print(response)
    lines = response.split("\n")
    
    obj = {}
    lines = response.split("\n")
    for line in lines:
        split = line.split(" ")
        #split = filter(None, split)
        for l in split:
            if l == directory:
                clean_line = list(filter(None, split))
                obj['total'] = clean_line[2]
                obj['used'] = clean_line[1]
                obj['total_bytes'] = get_bytes(obj['total'])
                obj['used_bytes'] = get_bytes(obj['used'])
                obj['used_percent'] = (obj['used_bytes'] / obj['total_bytes']) * 100
    
    return obj


def get_zfs_total(directory):
    data = get_zfs(directory)
    return get_bytes(data['total'])


def get_zfs_used(directory):
    data = get_zfs(directory)
    return get_bytes(data['used'])


def get_bytes(string):
    if "T" in string:
        raw = string.replace("T", "")   
        size = float(raw)
        return size*2**40
    elif "G" in string:
        raw = string.replace("G", "")   
        size = float(raw)
        return size*2**30
    elif "M" in string:
        raw = string.replace("M", "")   
        size = float(raw)
        return size*2**20
    elif "K" in string:
        raw = string.replace("K", "")   
        size = float(raw)
        return size*2**10
    else:
        return False


def get_hard_drives():
    response = run("lshw -class disk", True)
    disks = response.split("*-disk")
    array = []
    for disk in disks:
        obj = {}
        lines = disk.split("\n")
        for line in lines:
            if ":" in line:
                try:
                    value = line.split(": ")[1]
                    if "description:" in line:
                        obj['desc'] = value
                    elif "product:" in line:
                        obj['product'] = value
                    elif "vendor:" in line:
                        obj['vendor'] = value
                    elif "serial:" in line:
                        obj['serial'] = value
                    elif "size:" in line:
                        obj['size'] = value
                    elif "logical name:" in line:
                        obj['logical_name'] = value
                    else:
                        continue
                    
                except IndexError:
                    continue
        if obj:
            array.append(obj)
    return array


def get_smart(device):
    try:
        response = run("sudo smartctl --health " + device, True)
    except Exception:
        return False
    lines = response.split("\n")
    for line in lines:
        if "SMART Health Status:" in line:
            return line.split(": ")[1]
        elif "SMART overall-health" in line:
            return line.split(": ")[1]
    return False


def get_smart_report():
    response = get_hard_drives()
    array = []
    for drive in response:
        obj = {}
        obj = drive
        try:
            smart_result = get_smart(obj['logical_name'])
        except Exception:
            obj['status'] = "unknown"
            array.append(obj)
            continue
        obj['status'] = smart_result
        array.append(obj)
    return array

# -------------------------------------------------------------------------------------------------
# NETWORKING:
# -------------------------------------------------------------------------------------------------


def get_ips():
    try:
        response = run("ifconfig", True)
    except Exception:
        return False
    array = []
    # Interfaces
    interfaces = response.split("\n\n")
    interface_count = 0
    for interface in interfaces:
        obj = {}

        interface_count += 1
        #print("Interface Count: " + str(interface_count))
        #print(interface)

        # Break up Each Interface into Lines:
        lines = interface.split("\n")
        # Get Interface Name:
        obj['name'] = lines[0].split(" ")[0]
        # FILTER Loop Back
        if obj['name'] == "lo":
            continue

        # Get Interface Info:
        for line in lines:
            line_by_space = line.split(" ")
            # IP Address:
            if "inet" in line:
                for item in line_by_space:
                    if "addr:" in item:
                        obj['ip_v4'] = item.replace("addr:", "")
                        continue
                    if "inet " in item:
                        obj['ip_v4'] = item[1]
            # Ubuntu 14
            if "HWaddr" in line:
                obj['mac'] = line.split("HWaddr ")[1].rstrip(" ")
                continue
            # Ubunut 18
            if "ether " in line:
                obj['mac'] = line_by_space[1]
                continue

        # Add Object to Array:
        try:
            obj['name']
        except KeyError:
            continue

        # FILTER Empty or Interfaces with no Name:
        if not obj['name'] or obj['name'] is None:
            continue

        array.append(obj)
        continue

    return array
