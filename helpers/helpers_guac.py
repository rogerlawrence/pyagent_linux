import os
import globals
import helpers.helpers_virsh as helpers_virsh


def get_conn_temp(name, hostname, port, password, protocol="vnc"):
    """ FUNCTION get_conn_temp
        @param string name
        @param string hostname
        @param string port
        @param string password
        @param string protocol
    
        @return string template
    """

    template = '''
		<connection name="''' + name + '''">
	        	<protocol>''' + protocol + '''</protocol>
        		<param name="hostname">''' + hostname + '''</param>
	        	<param name="port">''' + port + '''</param>
	        	<param name="password">''' + password + '''</param>
		</connection>'''
    
    return template

def build_config(guac_user, guac_password):
    """ FUNCTION build_config
        @param string guac_user 
        @param string guac_password

        @return boolean
    """
    conn_string = ""
    # GET BDR IP ADDRESS
    bdr_ip = os.popen('ip addr show eth0 | grep "\<inet\>" | awk \'{ print $2 }\' | awk -F "/" \'{ print $1 }\'').read().strip()
    # GET CONNECTIONS
    connections = helpers_virsh.get_vms()
    # BUILD CONNECTIONS
    for conn in connections:
        conn_string += get_conn_temp(
            conn['id'],
            bdr_ip,
            conn['vnc_port'],
            conn['vnc_password']            
        )
    # ADD CONNECTIONS TO TEMPLATE

    template = '''
        <user-mapping>
	        <authorize username="''' + guac_user + '''" password="''' + guac_password + '''">
                ''' + conn_string + '''
        	
            </authorize>
        </user-mapping>'''

    #print(template)
    # WRITE TEMPLATE TO FILE
    try:
        file = open(globals.GM_CONFIG, 'w+')
        file.write(template)
    except Exception:
        return False
    return True
