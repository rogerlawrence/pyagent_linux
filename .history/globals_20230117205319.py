PYAGENT_VER = "1.026"

# SCREENCONNECT:
SC_PARAMS_FILE = "/opt/connectwisecontrol-54d9f2ec98463e54/ClientLaunchParameters.txt"
SC_PARAMS_FILE_OLD = "/opt/screenconnect-54d9f2ec98463e54/ClientLaunchParameters.txt"

# ZABBIX:
ZX_CONF_FILE = "/etc/zabbix/zabbix_agentd.conf"

# GUAC:
GM_CREATE = ["docker", "create", "--name", "gauc", "-p", "8080:8080", "-v", "/docker/guac:/config", "oznu/guacamole"]
GM_CONFIG = "/docker/guac/guacamole/user-mapping.xml"
GM_USER = "guacadmin"
GM_PASS = "guacadmin"

# CHARTEC
CT_RSSH_IP = "cp-connect.chartec.net"
CT_RSSH_PORT = "55555"
CT_AGENT_URL = "agent.chartec.net"

# DOWNLOADS
DL_SC = "https://cp.chartec.net/downloads/replibit/screenconnect.deb"
DL_ZX = "http://repo.zabbix.com/zabbix/3.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_3.2-1+xenial_all.deb"
DL_ZX_16 = "https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1%2Bxenial_all.deb"
DL_ZX_18 = "https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1%2Bbionic_all.deb"
DL_ZX_CONF = "https://cp.chartec.net/downloads/replibit/zx.txt"

#QEMU
QU_DIR = "/etc/libvirt/qemu/"

#ZFS
ZFS_DIR = "tank"
