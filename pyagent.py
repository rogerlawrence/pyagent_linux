#!/usr/bin/python3
import sys
import argparse
import globals
import json
import pprint
import helpers.helpers_os as helpers_os
import helpers.helpers_installers as helpers_installers
import helpers.helpers_tunnels as helpers_tunnels
import helpers.helpers_virsh as helpers_virsh
import helpers.helpers_guac as helpers_guac
from pathlib import Path
# -------------------------------------------------------------------------------------------------
# Arg Parse
# -------------------------------------------------------------------------------------------------
parser = argparse.ArgumentParser(description='CharTec Linux Agent')

parser.add_argument('--version', action='version', version=globals.PYAGENT_VER, help='version info')
parser.add_argument('--get', action='store', help='GET Calls', dest='get', nargs='+')
parser.add_argument('--create', action='store', help='CREATE Calls', dest='create', nargs='+')
parser.add_argument('--install', action='store', help='INSTALL Calls', dest='install')
parser.add_argument('--reset', action='store', help='RESET Calls', dest='reset')
parser.add_argument('--update', action='store', help='UPDATE Calls', dest='update')
parser.add_argument('--remove', action='store', help='REMOVE Calls', dest='remove')
parser.add_argument('--test', action='store', help='TEST Calls', dest='test', nargs='+')
# -------------------------------------------------------------------------------------------------
# End Arguments
# -------------------------------------------------------------------------------------------------
result = parser.parse_args()
#pprint.pprint(result)
# -------------------------------------------------------------------------------------------------
# GET FUNCTIONS:
# -------------------------------------------------------------------------------------------------
if result.get:

    if result.get[0] == "screenconnect":
        # -----------------------------------------------------------------------------------------
        # SCREENCONNECT:
        # -----------------------------------------------------------------------------------------
        try:
            if Path(globals.SC_PARAMS_FILE).exists():
                lines = [line.rstrip('\n') for line in open(globals.SC_PARAMS_FILE)][0]
                split_lines = lines.split("s=")[1]
                sc_guid = split_lines.split("&")[0]
            else:
                lines = [line.rstrip('\n') for line in open(globals.SC_PARAMS_FILE_OLD)][0]
                split_lines = lines.split("&s=")[1]
                sc_guid = split_lines.split("&")[0]
        except Exception as e:
            print(0)
            sys.exit()
        print(sc_guid)
        sys.exit()
    elif result.get[0] == "vms":
        result = helpers_virsh.get_vms()
        if result:
            print(json.dumps(result))
        sys.exit()
    elif result.get[0] == "vnc":
        results = helpers_virsh.get_vms()
        if results:
            for result in results:
                print("GUID: " + result['id'][:-13] + " PORT: " + result['vnc_port'] + "  PASSWORD: " + result['vnc_password'])
        sys.exit()
    elif result.get[0] == "zfs_tank":
        result = helpers_os.get_zfs(globals.ZFS_DIR)
        print(json.dumps(result))
        sys.exit()
    elif result.get[0] == "zfs_list":
        result = helpers_os.get_zfs_list(globals.ZFS_DIR)
        print(json.dumps(result))
        sys.exit()        
    elif result.get[0] == "zfs_total":
        result = helpers_os.get_zfs_total(globals.ZFS_DIR)
        print(result)
        sys.exit()
    elif result.get[0] == "zfs_used":
        result = helpers_os.get_zfs_used(globals.ZFS_DIR)
        print(result)
        sys.exit()
    elif result.get[0] == "disks":
        result = helpers_os.get_hard_drives()
        print(json.dumps(result))
        sys.exit()
    elif result.get[0] == "smart":
        error1 = "Please provide logical name.  Example: /dev/sdb"

        try:
            result.get[1]
        except IndexError:
            print(error1)
            sys.exit()

        if not result.get[1]:
            print(error1)
            sys.exit()
        else:
            result = helpers_os.get_smart(result.get[1])
            print(result)
            sys.exit()
    elif result.get[0] == "smart_report":
        result = helpers_os.get_smart_report()
        print(json.dumps(result))
        sys.exit()
    elif result.get[0] == "ips":
        result = helpers_os.get_ips()
        print(json.dumps(result))
        sys.exit()
    else:
        print("unknown")
        sys.exit()

# -------------------------------------------------------------------------------------------------
# CREATE FUNCTIONS:
# -------------------------------------------------------------------------------------------------
if result.create:
    # GUAC TUNNEL:
    if result.create[0] == "guac_tunnel":
        if result.create[1] and result.create[2]:
            source_port = result.create[1]
            dest_port = result.create[2]
        else:
            print("Error: Missing Port Options")
            sys.exit()
        result = helpers_guac.build_config(globals.GM_USER, globals.GM_PASS)
        tunnel = helpers_tunnels.tunnel_create(dest_port, source_port)
        if tunnel:
            print("OK")
            sys.exit()
        else:
            print("Error: Could not create Tunnel")
            sys.exit()
    elif result.create[0] == "guac_config":
        result = helpers_guac.build_config(globals.GM_USER, globals.GM_PASS)
        if result:
            print("Success")
        else:
            print("Failure")
        sys.exit()

    sys.exit()

# -------------------------------------------------------------------------------------------------
# INSTALL FUNCTIONS:
# -------------------------------------------------------------------------------------------------
if result.install:
    # SCREENCONNECT:
    if result.install == "screenconnect":
        install = helpers_installers.install_screenconnect()
        if install:
            print("Successful")
        else:
            print("Errors occurred during Install")
    elif result.install == "zabbix_18":
        install = helpers_installers.install_zabbix(18)
        if install:
            print("Successful")
        else:
            print("Errors occuring during Install")
    elif result.install == "zabbix_16":
        install = helpers_installers.install_zabbix(16)
        if install:
            print("Successful")
        else:
            print("Errors occurred during Install")
    elif result.install == "docker":
        install = helpers_installers.install_docker()
        if install:
            print("Successful")
        else:
            print("Errors occurred during Install")
    elif result.install == "guac":
        install = helpers_installers.install_guac()
        if install:
            print("Successful")
        else:
            print("Errors occurred during Install")
    elif result.install == "cronjob":
        install = helpers_installers.install_cronjob()
        if install:
            print("Cronjob for pyupdate version check Successful")
        else:
            print("Cronjob for pyupdate version check Errors occurred")           
    elif result.install == "all":
        install = helpers_installers.install_screenconnect()
        if install:
            print("ScreenConnect Install Successful")
        else:
            print("ScreenConnect Install Errors occurred")
        install = helpers_installers.install_zabbix(16)
        if install:
            print("Zabbix Install Successful")
        else:
            print("Zabbix Install Errors occurred")
        install = helpers_installers.install_docker()
        if install:
            print("Docker Install Successful")
        else:
            print("Docker Install Errors occurred")
        install = helpers_installers.install_guac()
        if install:
            print("Guac Install Successful")
        else:
            print("Guac Install Errors occurred")         
        install = helpers_installers.install_cronjob()
        if install:
            print("Cronjob for pyupdate version check Successful")
        else:
            print("Cronjob for pyupdate version check Errors occurred")   
    else:
        print("unknown")
        sys.exit()

# -------------------------------------------------------------------------------------------------
# RESET FUNCTIONS:
# -------------------------------------------------------------------------------------------------
if result.reset:
    if result.reset == "guac":
        helpers_installers.reset_guac()
    else:
        print("unknown")
        sys.exit()

# -------------------------------------------------------------------------------------------------
# UPDATE FUNCTIONS:
# -------------------------------------------------------------------------------------------------
if result.update:
    if result.update == "pyupdate":
        helpers_installers.check_pyupdate_version()
    else:
        print("unknown")
        sys.exit()

# -------------------------------------------------------------------------------------------------
# REMOVE FUNCTIONS:
# -------------------------------------------------------------------------------------------------
if result.remove:
    if result.remove == "guac":
        helpers_installers.remove_guac()
    else:
        print("unknown")
        sys.exit()

# -------------------------------------------------------------------------------------------------
#
# -------------------------------------------------------------------------------------------------
if result.test:
    if result.test[0] == "guac_installed":
        test = helpers_installers.get_guac_id()
        print(test)
    elif result.test[0] == "guac_running":
        result = helpers_installers.check_guac()
        if result:
            print("Guac is running.")
        else:
            print("Guac is NOT running.")
        sys.exit()
    elif result.test[0] == "virsh":
        result = helpers_virsh.get_vms()
        if result:
            print(json.dumps(result))
    elif result.test[0] == "guac_pop":
        result = helpers_guac.build_config(globals.GM_USER, globals.GM_PASS)
        sys.exit()
    elif result.test[0] == "smart":
        result = helpers_os.get_smart(result.test[1])
        print(result)
        sys.exit()